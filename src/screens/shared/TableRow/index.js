/* eslint-disable react/prop-types */
/* eslint-disable linebreak-style */
/* eslint-disable quotes */
import React from 'react';
import PropTypes from 'prop-types';

export default function TableRow({
  user,
  setSelectedUser,
  selectedUser,
  deleteUser,
}) {
  const handleUserDelete = () => {
    deleteUser(user);
  };
  return (
    <tr
      style={{
        backgroundColor:
          user.idUser == selectedUser.idUser ? 'yellow' : 'white',
      }}
      onClick={() => setSelectedUser(user)}
    >
      <td>{user.idUser}</td>
      <td>{user.firstName}</td>
      <td>{user.lastName}</td>
      <td>
        <button onClick={handleUserDelete}>Delete</button>
      </td>
    </tr>
  );
}

TableRow.propTypes = {
  user: PropTypes.object.isRequired,
  setSelectedUser: PropTypes.func.isRequired,
  selectedUser: PropTypes.shape({
    idUser: PropTypes.number,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
  deleteUser: PropTypes.func.isRequired,
};

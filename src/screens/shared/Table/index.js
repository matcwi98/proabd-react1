/* eslint-disable linebreak-style */
import React from 'react';
import PropTypes from 'prop-types';
import TableHeader from '../TableHeader';
import TableRow from '../TableRow';

export default function Table({
  users,
  setSelectedUser,
  selectedUser,
  setUsers,
}) {
  const [sortedUsers, setSortedUsers] = React.useState(users);
  const deleteUser = (user) => {
    users.splice(users.indexOf(user), 1);
  };

  const handleSortData = () => {
    setUsers(
      [...users].sort((a, b) => {
        if (a < b) return 1;
        return -1;
      })
    );
  };
  return (
    <table>
      <button onClick={handleSortData}>SORT USERS</button>
      <TableHeader columnsNames={['id user', 'first name', 'last name']} />
      <tbody>
        {users.map((u) => (
          <TableRow
            key={u.idUser}
            user={u}
            selectedUser={selectedUser}
            setSelectedUser={setSelectedUser}
            deleteUser={deleteUser}
          />
        ))}
      </tbody>
    </table>
  );
}

Table.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  setSelectedUser: PropTypes.func.isRequired,
  selectedUser: PropTypes.shape({
    idUser: PropTypes.number,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
};
